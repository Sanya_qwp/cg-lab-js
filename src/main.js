import Vue from 'vue'
import App from './App.vue'

import Vuex from 'vuex'
Vue.use(Vuex)
import store from './store/store.js'

import 'normalize.css'

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
